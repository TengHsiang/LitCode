#include "../include/linkedList.hpp"

class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        int nextVal = 0;
        ListNode* curNode = new ListNode;
        ListNode* l3 = new ListNode;
        l3->next = curNode;
        
        while(l1 != nullptr || l2 != nullptr) {
            ListNode *nextNode = new ListNode(nextVal);
            
            if(l1 != nullptr) {
                nextNode->val += l1->val;
                l1 = l1->next;
            }
            if(l2 != nullptr) {
                nextNode->val += l2->val;
                l2 = l2->next;
            }
            
            curNode->val = nextNode->val%10;
            nextVal = (nextNode->val>9)?1:0;
            
            if(l1==nullptr && l2==nullptr) {
                curNode->next = nullptr;
                if(nextVal == 1) {
                    nextNode->val = 1;
                    curNode->next = nextNode;
                }
            }
            else {
                curNode->next = nextNode;
                curNode = curNode->next;
            }
        }
        
        return l3->next;
    }
    ListNode* addTwoNumbersRef(ListNode* l1, ListNode* l2) {
        int sum=0;
        ListNode *l3=NULL;
        ListNode **node=&l3;
        while(l1!=NULL||l2!=NULL||sum>0)
        {
            if(l1!=NULL)
            {
                sum+=l1->val;
                l1=l1->next;
            }
            if(l2!=NULL)
            {
                sum+=l2->val;
                l2=l2->next;
            }
            (*node)=new ListNode(sum%10);
            sum/=10;
            node=&((*node)->next);
        }        
        return l3;
    }

};
#define CATCH_CONFIG_MAIN
#include "../Catch2/single_include/catch2/catch.hpp"
#include "solution.hpp"

TEST_CASE("AddTwoNumbers", "addTwoNumbers")
{
    Solution s;
    {
        const int len1 = 7;
        int nums1[len1] = {9,9,9,9,9,9,9};
        ListNode* list1 = createList(nums1, len1);

        const int len2 = 4;
        int nums2[len2] = {9,9,9,9};
        ListNode* list2 = createList(nums2, len2);

        const int len3 = 8;
        int nums3[len3] = {8,9,9,9,0,0,0,1};
        ListNode* list3 = createList(nums3, len3);

        REQUIRE( bitTrueList(s.addTwoNumbers(list1, list2), list3) );
    }

    {
        ListNode* list1 = new ListNode(0);
        ListNode* list2 = nullptr;
        ListNode* list3 = new ListNode(0);

        REQUIRE( bitTrueList(s.addTwoNumbers(list1, list2), list3) );
    }
}
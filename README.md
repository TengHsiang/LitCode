# LitCode :fire: :fire: :fire:
It is a definitely boring repo to record my solutions

# Usage
Compile with [Catch2](https://github.com/catchorg/Catch2)
```bash
$ ./exec.sh --run <folder-name>
```

# 60 Problems to Solve for Coding Interview
## Table of Contents
1. [Linked List](https://gitlab.com/TengHsiang/LitCode#linked-list)</br>
2. [Stack](https://gitlab.com/TengHsiang/LitCode#stack)</br>
3. [Heap, Priority Queue](https://gitlab.com/TengHsiang/LitCode#heap-priority-queue)</br>
4. [Hash Map](https://gitlab.com/TengHsiang/LitCode#hash-map)</br>
5. [Graph, BFS, DFS](https://gitlab.com/TengHsiang/LitCode#graph-bfs-dfs)</br>
6. [Tree, BST](https://gitlab.com/TengHsiang/LitCode#tree-bst)</br>
7. [Dynamic Programming](https://gitlab.com/TengHsiang/LitCode#dynamic-programming)</br>
8. [Binary Search](https://gitlab.com/TengHsiang/LitCode#binary-search)</br>
9. [Recursion](https://gitlab.com/TengHsiang/LitCode#Recursion)</br>
10. [Sliding Window](https://gitlab.com/TengHsiang/LitCode#sliding-window)</br>
11. [Greedy, Backtracking](https://gitlab.com/TengHsiang/LitCode#greedy-backtracking)</br>
12. [Others](https://gitlab.com/TengHsiang/LitCode#others)</br>
13. [Reference](https://leetcode.com/list/xo2bgr0r)</br>

## Linked List
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [141](https://leetcode.com/problems/linked-list-cycle/) | Linked List Cycle | Easy |   |   |
| [142](https://leetcode.com/problems/linked-list-cycle-ii/) | Linked List Cycle II | Medium |   |   |
| [83](https://leetcode.com/problems/remove-duplicates-from-sorted-list/) | Remove Duplicates from Sorted List | Easy | 2020/12/09 | 練習**pointer |
| [82](https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/description/) | Remove Duplicates from Sorted List II | Medium |   | 記得[Linus有品味指標](https://github.com/mkirchner/linked-list-good-taste.git) |
| [2](https://leetcode.com/problems/add-two-numbers/) | Add Two Numbers | Medium | 2020/12/09 | 注意兩個link的next link都為NULL情況，並且有進位（carry)的情形。 使用**pointer |
</br>

## Stack
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [20](https://leetcode.com/problems/valid-parentheses/) | Valid Parentheses | Easy |   | 注意最後stack是否還有元素 |
| [206](https://leetcode.com/problems/reverse-linked-list/) | Reverse Linked List | Easy |   | 可以使用stack FILO的特性 |
</br>

## Heap, Priority Queue
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [703](https://leetcode.com/problems/kth-largest-element-in-a-stream/) | Kth Largest Element in a Stream | Easy |   | Priority_queue 的使用方法</br>1. priority_queue 使用top</br>2. queue使用 front |
| [347](https://leetcode.com/problems/top-k-frequent-elements/) | Top K Frequent Elements | Medium |   | unordered_map 與priority_queue + mysort組合 |
| [373](https://leetcode.com/problems/find-k-pairs-with-smallest-sums/) | Find K Pairs with Smallest Sums | Easy |   | 可以使用stack FILO的特性 |
</br>

## Hash Map
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [1](https://leetcode.com/problems/two-sum/) | Two Sum | Easy |   |  |
| [49](https://leetcode.com/problems/group-anagrams/) | Group Anagrams | Medium |   | 雖然用histogram可以解但太慢，string sorting 會更好 |
| [349](https://leetcode.com/problems/intersection-of-two-arrays/) | Intersection of Two Arrays | Easy |   | unordered_set 使用，且注意到移除element的方法是erase |
| [929](https://leetcode.com/problems/unique-email-addresses/) | Unique Email Addresses | Easy |   | 結合string的使用方法，注意到substr以及append用法 |
| [387](https://leetcode.com/problems/first-unique-character-in-a-string/) | First Unique Character in a String | Easy |   | 別一開始就想著用map，其實list快多了 |
| [560](https://leetcode.com/problems/subarray-sum-equals-k/) | Subarray Sum Equals K | Medium |   | 重要！cumulative sum + haspmap  |
| [138](https://leetcode.com/problems/copy-list-with-random-pointer/) | Copy List with Random Pointer | Medium |   | 建立原本的node pointer與新的node pointer關係，透過hashmap</br>Amazon Ring考題 |
</br>

## Graph, BFS, DFS
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [200](https://leetcode.com/problems/number-of-islands/) | Number of Islands | Medium |   | DFS, 也就是connected component image 概念 |
| [695](https://leetcode.com/problems/max-area-of-island/) | Max Area of Island | Medium |   |   |
| [Locked](https://leetcode.com/problems/number-of-connected-components-in-an-undirected-graph/) | Number of Connected Components in an Undirected Graph | Medium |   |  |
| [127](https://leetcode.com/problems/word-ladder/) | Word Ladder | Medium |   |   |
</br>

## Tree, BST
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [104](https://leetcode.com/problems/maximum-depth-of-binary-tree/) | Maximum Depth of Binary Tree | Easy |  | 2行結束 |
| [111](https://leetcode.com/problems/minimum-depth-of-binary-tree/) | Minimum Depth of Binary Tree | Easy |  |  |
| [617](https://leetcode.com/problems/merge-two-binary-trees/) | Merge Two Binary Trees | Easy |  | 嘗試使用O(1)空間複雜度 |
| [108](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/) | Convert Sorted Array to Binary Search Tree | Easy |  | array 的indexing 範圍表示方法. 注意recursive 的條件 |
| [112](https://leetcode.com/problems/path-sum/) | Path Sum | Easy |  | 注意到他是從root 到leaf，不用考慮sum tree |
| [102](https://leetcode.com/problems/binary-tree-level-order-traversal/) | Binary Tree Level Order Traversal | Medium |  | 使用queue達到BFS，要結合queue 的size判斷同level有幾個node |
| [103](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/) | Binary Tree Zigzag Level Order Traversal | Medium |  | 跟Level Order想法相同，差在用一個bool判斷reverse，以及insert的用法 |
| [98](https://leetcode.com/problems/validate-binary-search-tree/) | Validate Binary Search Tree | Medium |  | 記得BST特性：inorder的順序是要排序過的 |
| [105](https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/) | Construct Binary Tree from Preorder and Inorder Traversal | Medium |  | 觀念題 |
| [543](https://leetcode.com/problems/diameter-of-binary-tree/) | Diameter of Binary Tree | Easy |  | Google 考題 |
</br>

## Dynamic Programming
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [Locked](https://leetcode.com/problems/paint-fence/) | Paint Fence | Easy |  |  |
| [300](https://leetcode.com/problems/longest-increasing-subsequence/) | Longest Increasing Subsequence | Medium |  |  |
| [53](https://leetcode.com/problems/maximum-subarray/) | Maximum Subarray | Easy |  |  |
| [62](https://leetcode.com/problems/unique-paths/) | Unique Paths | Medium |  |  |
| [63](https://leetcode.com/problems/unique-paths-ii/) | Unique Paths II | Medium |  |  |
| [198](https://leetcode.com/problems/house-robber/) | House Robber | Easy |  |  |
| [213](https://leetcode.com/problems/house-robber-ii/) | House Robber II | Medium |  |  |
| [121](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/) | Best Time to Buy and Sell Stock | Easy |  |  |
| [122](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/) | Best Time to Buy and Sell Stock II | Easy |  | Object: Find the subset |
| [139](https://leetcode.com/problems/word-break/) | Word Break | Medium |  |  |
| [322](https://leetcode.com/problems/coin-change/) | Coin Change  | Medium |  |  |
</br>

## Binary Search
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [35](https://leetcode.com/problems/search-insert-position/) | Search Insert Position | Easy |  | 要注意while截止條件，lo <= hi，是小於等於，每次更新都是從mid +/- 1 |
| [153](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/) | Find Minimum in Rotated Sorted Array | Medium |  | 可以使用recursive概念. 建議用右邊的方法來實現 [linked](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/discuss/48493/Compact-and-clean-C%2B%2B-solution) |
| [33](https://leetcode.com/problems/search-in-rotated-sorted-array/) | Search in Rotated Sorted Array | Medium |  | 可以結合上一提的做法，先找到pivot point |
| [1011](https://leetcode.com/problems/capacity-to-ship-packages-within-d-days/) | Capacity To Ship Packages Within D Days | Medium |  | 變化題，難！ |
</br>

## Recursion
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [50](https://leetcode.com/problems/powx-n/) | Pow(x, n) | Medium |  |  |
| [779](https://leetcode.com/problems/k-th-symbol-in-grammar/) | K-th Symbol in Grammar | Medium |  |  |
| [Locked](https://leetcode.com/problems/split-bst/) | Split BST | Medium |  |  |
</br>

## Sliding Window
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [3](https://leetcode.com/problems/longest-substring-without-repeating-characters/) | Longest Substring Without Repeating Characters | Medium |  | unordered_set + slide window |
| [209](https://leetcode.com/problems/minimum-size-subarray-sum/) | Minimum Size Subarray Sum | Medium |  | 截止條件的順序 |
</br>

## Greedy, Backtracking
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [46](https://leetcode.com/problems/permutations/) | Permutations | Easy |  | Backtracking，記得用swap |
| [78](https://leetcode.com/problems/subsets/) | Subsets | Medium |  | 注意到使用index 去濾除重複element |
| [39](https://leetcode.com/problems/combination-sum/) | Combination Sum | Medium |  |  |
| [22](https://leetcode.com/problems/generate-parentheses/) | Generate Parentheses | Medium |  |  |
</br>

## Others
| Indices | Problems | Difficulty | Solved Dates | Comments |
|:-:|---|---|---|---|
| [283](https://leetcode.com/problems/move-zeroes/) | Move Zeroes | Easy |  | 用一個pointer 去指向current valid number |
| [252](https://leetcode.com/problems/meeting-rooms/) | Meeting Rooms | Easy |  |  |
| [253](https://leetcode.com/problems/meeting-rooms-ii/) | Meeting Rooms II | Medium |  |  |
| [392](https://leetcode.com/problems/is-subsequence/) | Is Subsequence | Easy |  | 運用pointer 去檢查每一個元素 |
| [31](https://leetcode.com/problems/next-permutation/) | Next Permutation | Medium |  |  |
| [8](https://leetcode.com/problems/string-to-integer-atoi/) | String to Integer (atoi) | Medium |  |  |
| [6](https://leetcode.com/problems/zigzag-conversion/) | ZigZag Conversion | Medium |  |  |
| [15](https://leetcode.com/problems/3sum/) | 3 sum  | Medium |  | 注意到要檢查重複元素 (Facebook) |
</br>

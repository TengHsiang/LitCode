#pragma once
#include <iostream>

// Definition for singly-linked list.
typedef struct ListNode {
    int val;
    ListNode *next;
    ListNode() : val(0), next(nullptr) {}
    ListNode(int x) : val(x), next(nullptr) {}
    ListNode(int x, ListNode *next) : val(x), next(next) {}
} ListNode;

ListNode* createList(int* nums, int len) {
    ListNode* head = NULL;
    ListNode* tail = NULL;

    for (int i = 0; i < len; i++) {
        if (head == NULL) {
            head = new ListNode(nums[i]);
            tail = head;
        } else {
            tail->next = new ListNode(nums[i]);
            tail = tail->next;
        }
    }

    return head;
}

bool bitTrueList(ListNode *l1, ListNode *l2) {
    while(l1 != nullptr || l2 != nullptr) {
        if(l1 == nullptr) return false; 
        if(l2 == nullptr) return false;
        if(l1->val != l2->val) return false;

        l1 = l1->next;
        l2 = l2->next;
    }
    return true;
}

bool arrayEqualsList(int* nums, int len, ListNode* list) {
    for (int i = 0; i < len; i++) {
        if (list == NULL || list->val != nums[i]) {
            return false;
        }
        list = list->next;
    }

    return true;
}

int getListValueAt(ListNode* root, int pos) {
    for (int i = 0; i < pos; i++) {
        root = root->next;
    }

    return root->val;
}

void printList(ListNode** list) {
    if (list != NULL) {
        std::cout << "[List] ";
        ListNode* p = *list;
        while (p) {
            std::cout << p->val << " ";
            p = p->next;
        }
        std::cout << std::endl;
    }
    else {
        std::cout << "[ERR] list is null" << std::endl;
    }
}
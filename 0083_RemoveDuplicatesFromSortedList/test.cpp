#define CATCH_CONFIG_MAIN
#include "../Catch2/single_include/catch2/catch.hpp"
#include "solution.hpp"

TEST_CASE("DeleteDuplicates", "deleteDuplicates")
{
    Solution s;
    {
        const int len1 = 4;
        int nums1[len1] = {1,1,1,2};
        ListNode* list1 = createList(nums1, len1);
        // printList(&list1);

        const int len3 = 2;
        int nums3[len3] = {1,2};
        ListNode* list3 = createList(nums3, len3);

        REQUIRE( bitTrueList(s.deleteDuplicates(list1), list3) );
    }

    {
        const int len1 = 5;
        int nums1[len1] = {1,1,2,3,3};
        ListNode* list1 = createList(nums1, len1);

        const int len3 = 3;
        int nums3[len3] = {1,2,3};
        ListNode* list3 = createList(nums3, len3);

        REQUIRE( bitTrueList(s.deleteDuplicates(list1), list3) );
    }

    {
        ListNode* list1 = nullptr;
        ListNode* list3 = nullptr;

        REQUIRE( bitTrueList(s.deleteDuplicates(list1), list3) );
    }
}
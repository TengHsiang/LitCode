#include "../include/linkedList.hpp"

class Solution {
public:
    ListNode* deleteDuplicates(ListNode* head) {
        #if 0
        ListNode *ansList = new ListNode(0, head);
        ListNode *preNode = new ListNode();
        int first = 1;
        
        while(head != nullptr) {
            if(first == 1) {
                preNode = head;
                head = head->next;
                first = 0;
                continue;
            }
            
            if(head->val == preNode->val) {
                preNode->next = (head->next)?head->next:nullptr;
            } else {
                preNode = head;    
            }
            
            head = head->next;
        }
        
        return ansList->next;
        #else

        ListNode **list = &head;
            
        while((*list) != nullptr && (*list)->next != nullptr) {
            
            if((*list)->val == (*list)->next->val) {
                (*list)->next = (*list)->next->next;
                continue;
            }
            else {
                list = &(*list)->next;
            }
        }
        printList(&head); // correct result
        printList(list); // the last node in head list
        return head;
        #endif
    }
};
#include <stdio.h>
#include <stdlib.h>

// A linked list (LL) node to store a queue entry
struct node_t {
	int data;
    struct node_t *next;
};

struct queue_t {
    struct node_t *front;
    struct node_t *rear;
};

// A utility function to create a new linked list node.
struct node_t *newNode (int k) {
    struct node_t *temp = (struct node_t *)malloc(sizeof(struct node_t));
	temp->data = k;
    temp->next = NULL;
	return temp;
}

// A utility function to create an empty queue
struct queue_t *createQueue (void) {
    struct queue_t *q = (struct queue_t *)malloc(sizeof(struct queue_t));
	q->front = q->rear = NULL;
	return q;
}

// The function to add a key k to q
void enQueue (struct queue_t *q, int k)
{
    // Create a new LL node
    struct node_t *temp = newNode(k);
  
    // If queue is empty, then new node is front and rear both
    if (q->rear == NULL) {
        q->front = q->rear = temp;
        return;
    }
  
    // Add the new node at the end of queue and change rear
    q->rear->next = temp;
    q->rear = temp;
}
  
// Function to remove a key from given queue q
void deQueue (struct queue_t *q)
{
    // If queue is empty, return NULL.
    if (q->front == NULL)
        return;
  
    // Store previous front and move front one node ahead
    struct node_t *temp = q->front;
  
    q->front = q->front->next;
  
    // If front becomes NULL, then change rear also as NULL
    if (q->front == NULL)
        q->rear = NULL;
  
    free(temp);
}

void printQueue (struct queue_t *q) {
    struct node_t *temp = q->front;
    do {
        printf("%d ", temp->data);
        temp = temp->next;
    } while (temp != NULL);
    printf("\n");
}

void insertQueue (struct queue_t *self, struct node_t *inNode, const int idx) {
    struct node_t *localNode = self->front;
	int cnt = 0;
    
    while(cnt+1 < idx && localNode != NULL) {
		localNode = localNode->next;
		cnt++;
	}
    
    if(localNode == NULL) {
        enQueue(self, inNode->data);
        return;
    }
	
	struct node_t *tmp = localNode->next;
	localNode->next = inNode;
	inNode->next = tmp;
}

void deleteQueue (struct queue_t *self, const int idx) {
    struct node_t *localNode = self->front;
    int cnt = 0;
    
    while(cnt+1 < idx && localNode != NULL) {
        localNode = localNode->next;
        cnt++;
    }
    
    if(localNode == NULL) {
        printf("[Err] dequeue out of queue size\n");
        return;
    }
    
    localNode->next = localNode->next->next;
}

struct node_t *getNodeFromQueue (struct queue_t *self, const int idx) {
    struct node_t *localNode = self->front;
    int cnt = 0;
    
    while(cnt < idx && localNode != NULL) {
        localNode = localNode->next;
        cnt++;
    }
    
    if(localNode == NULL) {
        printf("[Err] getNodeFromQueue out of queue size\n");
        return NULL;
    }
    
    localNode->next = NULL;
    
    return localNode;
}

int main(int argc, char *argv[]) {
    struct queue_t *q = createQueue();
    struct node_t *node = newNode(150);
    int i = 0;
    
    for(i = 0; i < 10; i++) {
        enQueue(q, i*10);
    }
    insertQueue(q, node, 4);
    insertQueue(q, node, 15); // insert a node outside the queue
    deleteQueue(q, 3);
    deleteQueue(q, 20);
    printQueue(q);
    struct node_t *getNode = getNodeFromQueue(q, 9);
    printf("get node: %d\n", getNode->data);
    return 0;
}

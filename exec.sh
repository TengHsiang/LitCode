#!/bin/bash

source Scripts/ansi.sh

if [[ $# < 2 ]]; then
	echo -e ""  
	echo -e "${LightGreen}\t--run [folder_name]${NC}"
	echo -e ""
	echo -e "${LightGreen}\t--run_clean [folder_name]${NC}"
	echo -e ""   
else
	if [[ $1 == "--run" ]]; then
		cd $2
		g++ -std=c++11 -fsanitize=address -Wall *.cpp -o $2.out
		./$2.out
	elif [[ $1 == "--run_clean" ]]; then
		rm $2/*.out
	fi
fi
